/*%{
  var tokenize = function(type, col) {
    return [type + ":", col];
  }
  var group = function(type, members) {
    return [type + ":", members];
  }
%}
*/
%lex

%%

\s+                               return 'TOKEN_SPACE';
"sid"                             { return 'TOKEN_NOUN'; }
"mahesh"                          { return 'TOKEN_NOUN'; }
"dolly"                           { return 'TOKEN_NOUN'; }
"chicken"                         { return 'TOKEN_FOOD'; }
"milk"                            { return 'TOKEN_FOOD'; }
"fish"                            { return 'TOKEN_FOOD'; }
"eats"                            { return 'TOKEN_VERB'; }
"hates"                           { return 'TOKEN_VERB'; }
"likes"                           { return 'TOKEN_VERB'; }
<<EOF>>                           { return 'TOKEN_EOF'; }

/lex

%start PARAGHAPH

%%

PARAGHAPH
  : SENTENCES SPACE EOF
  {{
      return $$;
  }}
  ;

SENTENCES
  : SENTENCE
  {{
    $$ = [$1];
  }}
  | SENTENCES SPACE SENTENCE
  {{
    $$ = $1.concat($3);
  }}
  ;

SENTENCE
  : NOUN SPACE VERB SPACE FOOD
  {{
      $$ = { 'NOUN': $1, 'VERB': $3, 'FOOD': $5 }
  }}
  ;

NOUN
  : TOKEN_NOUN
  {{
      $$ = $1
  }}
  ;
FOOD
  : TOKEN_FOOD
  {{
      $$ = $1
  }}
  ;
VERB
  : TOKEN_VERB
  {{
      $$ = $1
  }}
  ;
EOF
  : TOKEN_EOF
  {{
      $$ = $1
  }}
  ;
SPACE
  : TOKEN_SPACE
  {{
      $$ = $1
  }}
  ;
