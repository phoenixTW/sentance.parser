var sentance = require('./simple-sentence.js');
var sentenceTense = require('./sentence-tense');
var _ = require('lodash');
var fs = require('fs');

var inputFile = process.argv[2];
var option = process.argv[3];

if (_.isUndefined(inputFile)) {
  console.log("No input file has been passed.")
  process.exit(0);
}

var inputText = fs.readFileSync(inputFile, 'utf8');
var tokens = sentance.parse(inputText);


var generate = {
  SENTENCE: function (data) {
    var sentence = data.NOUN;
    if(_.last(option.split('-')) == 'continuous') sentence += " was";
    return sentence + " " + data.VERB + " " + data.FOOD;
  },
  SENTENCES: function(sentenceCollection) {
    return sentenceCollection.map(function(sentence){
      return generate.SENTENCE(sentence);
    }).join("\n");
  }
}

var changeTo = {
  past: function(ast, type, verbTenses) {
    return ast.map(function(sentanceToken) {
      var token = sentanceToken[type];
      var tokenVerbs = verbTenses[_.findIndex(verbTenses, { "present-third-person": token })];
      return {'NOUN': sentanceToken.NOUN, 'VERB': tokenVerbs['simple-past'], 'FOOD': sentanceToken.FOOD };
    });
  },
  "past-continuous": function(ast, type, verbTenses) {
    return ast.map(function(sentanceToken) {
      var token = sentanceToken[type];
      var tokenVerbs = verbTenses[_.findIndex(verbTenses, { "present-third-person": token })];
      return {'NOUN': sentanceToken.NOUN, 'VERB-TO-BE': "was", 'VERB': tokenVerbs['continuous'], 'FOOD': sentanceToken.FOOD };
    });
  }
}

var convertedSentenceTokens = changeTo[option](tokens, 'VERB', sentenceTense);
fs.writeFileSync('simple-past.sen', generate.SENTENCES(convertedSentenceTokens), 'utf8');
