%{
  var tokenize = function(type, col) {
    return [type + ":", col];
  }
  var group = function(type, members) {
    return [type + ":", members];
  }
%}

%lex

%%

\s+                               return 'TOKEN_SPACE';
"sid"                             { return 'TOKEN_NOUN'; }
"mahesh"                          { return 'TOKEN_NOUN'; }
"dolly"                           { return 'TOKEN_NOUN'; }
"chicken"                         { return 'TOKEN_FOOD'; }
"milk"                            { return 'TOKEN_FOOD'; }
"fish"                            { return 'TOKEN_FOOD'; }
"eats"                            { return 'TOKEN_VERB'; }
"hates"                           { return 'TOKEN_VERB'; }
"likes"                           { return 'TOKEN_VERB'; }
<<EOF>>                           { return 'TOKEN_EOF'; }

/lex

%start PROGRAM

%%

PROGRAM
  : EXPRESSION SPACE EOF
  {{
      $$ = group('PROGRAM', [$1])
      return $$;
  }}
  ;

EXPRESSION
  : SENTENCE
  {{
    $$ = group('EXPRESSION',[$1]);
  }}
  | EXPRESSION SPACE SENTENCE
  {{
    $$ = group('EXPRESSION',[$1,$3]);
  }}
  ;

SENTENCE
  : NOUN SPACE VERB SPACE FOOD
  {{
      $$ = group('SENTENCE', [$1, $3, $5])
  }}
  ;

NOUN
  : TOKEN_NOUN
  {{
      $$ = tokenize('NOUN',yytext);
  }}
  ;
FOOD
  : TOKEN_FOOD
  {{
      $$ = tokenize('FOOD',yytext);
  }}
  ;
VERB
  : TOKEN_VERB
  {{
      $$ = tokenize('VERB',yytext);
  }}
  ;
EOF
  : TOKEN_EOF
  {{
      $$ = tokenize('EOF',yytext);
  }}
  ;
SPACE
  : TOKEN_SPACE
  {{
      $$ = tokenize('SPACE',yytext);
  }}
  ;
